# TODO

- [X] Implement saving features
  - [X] Implement "Save Game" feature
  - [X] Implement "Load Game" feature
  - [X] Implement "New Game" feature
  - [X] Use the above features in QtApp
  - [X] Use the above features in ConsoleApp
- [ ] Implement Dames
- [X] Implement QtApp
- [X] Refactoring
  - [X] Add class Position in custom_types
  - [X] check_winning_... functions should only take an index instead of a position
  - [X] check_winning_... functions should separate the retrieval of the cells from the checking of the cells
  - [X] <important!> GameLogic should contain the grid in protected. Put the methods that act on the grid and the grid itself at the same place
  - [X] <important!> Remove the Game class. GameLogic should take care of what Game does. Eventually, rename GameLogic in Game, MorpionLogic in Morpion, etc.
