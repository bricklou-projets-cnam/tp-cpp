# Projet C++

## Présentation
Ce projet propose une implémentation en C++ de plusieurs jeux de plateau. Il est possible de jouer aux jeux suivants :
- [Morpion](https://fr.wikipedia.org/wiki/Morpion_(jeu))
- [Puissance 4](https://fr.wikipedia.org/wiki/Puissance_4)
- [Othello](https://fr.wikipedia.org/wiki/Othello_(jeu))
- [Dames](https://fr.wikipedia.org/wiki/Dames)

Vous pouvez y jouer en mode graphique (par défaut) ou en mode console (option `--no-gui`).

## Auteurs
- [Samy MOSA](https://gitlab.com/samymosa02)
- [Louis Bailleau](https://gitlab.com/bricklou)
- [Isamettin AYDIN](https://gitlab.com/aisamet)
- [Younes BOKHARI](https://gitlab.com/ybokhari)

## Prérequis
- gcc >= 9.4
- make >= 4.2
- CMake >= 3.5
- Qt >= 5.12

## Compilation
```sh
mkdir -p build
cd build
cmake .. # `-G Ninja` pour utiliser ninja
make # ou ninja
```
Un Dockerfile est également disponible pour compiler le projet dans un conteneur.

## Exécution
Une fois compilé, le binaire se trouve dans `build/src/`.
```sh
cd build
./src/projet-cpp [--no-gui]
```
