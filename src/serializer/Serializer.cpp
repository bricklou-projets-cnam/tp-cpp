#include "Serializer.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "custom_types/vector/vector_serialiser.h"
#include "factories/game_factory/GameFactory.h"
#include "models/game/Game.h"
#include <filesystem>
#include <fstream>
#include <ios>
#include <iostream>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <vector>

// Sérialisation de l'objet dans un fichier
void Serializer::save(const std::unique_ptr<Game> &game,
                      const std::string &filename) {
  auto parent_dir = std::filesystem::path{filename}.parent_path();

  if (!std::filesystem::exists(parent_dir)) {
    std::filesystem::create_directories(parent_dir);
  }

  std::ofstream out(filename, std::ios::out | std::ios::binary);

  if (!out) {
    throw std::runtime_error("Cannot open the save file");
  }

  SaveData data{
      .type = game->get_game_type(),
      .players = game->get_players(),
      .grid = game->get_grid(),
      .current_player_index = game->get_current_player_index(),
  };

  out << data;

  if (!out.good()) {
    throw std::runtime_error("Failed to write save file");
  }

  out.close();
}

SaveData Serializer::load(const std::string &filename) {
  auto parent_dir = std::filesystem::path{filename}.parent_path();

  if (!std::filesystem::exists(parent_dir)) {
    std::filesystem::create_directories(parent_dir);
  }

  std::ifstream in(filename, std::ios::in | std::ios::binary);

  if (!in) {
    throw std::runtime_error("Cannot open the save file");
  }

  SaveData data{};
  in >> data;
  in.close();

  if (!in.good()) {
    throw std::runtime_error("Failed to read save file");
  }

  return data;
}
