#include "custom_types/game_type/GameType.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "custom_types/vector/Vector2D.h"
#include "models/game/Game.h"
#include "models/grid/Grid.h"
#include <fstream>

class Serializer {
  public:
    static void save(const std::unique_ptr<Game> &game,
                     const std::string &filename);
    static SaveData load(const std::string &filename);
};