#include "app/App.h"
#include "argument_parser/ArgumentParser.h"
#include "factories/app_factory/AppFactory.h"
#include <memory>

int main(int argc, char *argv[]) {
  std::unique_ptr<App> app = AppFactory::create_app(argc, argv);
  app->run();
  return 0;
}
