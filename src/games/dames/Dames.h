#pragma once

#include "models/game/Game.h"
#include <memory>
#include <optional>

class Dames : public Game {
  public:
    Dames(std::vector<Player> &players);
    void initialize_grid() override;
    void play_round(Position pos) override;
    bool is_over() const override;
    bool is_draw() const override;
    uint32_t get_winner_id() const override;
    uint32_t get_next_player_index() const override;
    bool check_user_move(Position pos) const override;

  private:
    void initialize_grid_row(uint32_t row, uint32_t symbol, uint32_t offset);
    bool is_initial_position_valid(const Position &position) const;
    bool is_move_position_valid(const Position position) const;
    bool is_move_position_valid(const Position posA, const Position posB) const;
    void select_pawn(const Position pos);
    void move_pawn(const Position pos);
    bool can_move(const Position pos) const;
    bool can_jump_to(const Position posA, const Position posB) const;
    bool is_on_diagonal(const Position posA, const Position posB, int dist) const;
    std::optional<Position> selected_position = std::nullopt;
};