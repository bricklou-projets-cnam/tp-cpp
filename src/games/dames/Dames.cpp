#include "Dames.h"
#include "custom_types/vector/Vector2D.h"
#include <iostream>
#include <limits>
#include <memory>
#include <string>

Dames::Dames(std::vector<Player> &players) : Game(players) {
  initialize_grid();
}

void Dames::initialize_grid() {
  grid = std::make_unique<Grid>(10, 10);
  // Placement pions joueur A
  initialize_grid_row(0, 2, 1);
  initialize_grid_row(1, 2, 0);
  initialize_grid_row(2, 2, 1);
  initialize_grid_row(3, 2, 0);
  // Placement pions joueur B
  initialize_grid_row(6, 1, 1);
  initialize_grid_row(7, 1, 0);
  initialize_grid_row(8, 1, 1);
  initialize_grid_row(9, 1, 0);
}

void Dames::initialize_grid_row(uint32_t row, uint32_t symbol,
                                uint32_t offset) {
  for (uint32_t column = 0; column < grid->get_columns_number(); column++) {
    if (column % 2 == offset) {
      grid->set_symbol(Position(row, column), symbol);
    }
  }
}

bool Dames::check_user_move(Position pos) const {
  return selected_position.has_value() ? is_move_position_valid(pos)
                                       : is_initial_position_valid(pos);
}

bool Dames::is_initial_position_valid(const Position &position) const {
  return grid->is_position_valid(position) &&
         grid->get_symbol(position) == get_current_player().get_id() &&
         can_move(position);
}

bool Dames::is_move_position_valid(const Position position) const {
  if (!grid->is_position_valid(position) || !grid->is_empty(position)) {
    return false;
  }
  return is_move_position_valid(selected_position.value(), position);
}

bool Dames::is_move_position_valid(const Position posA,
                                   const Position posB) const {
  if (grid->is_empty(posA) || !grid->is_empty(posB)) {
    return false;
  }
  uint32_t symbol = grid->get_symbol(posA);
  switch (symbol) {
    case 1:
    case 3:
      return posB == posA + Vector2D{1, -1} ||
             posB == posA + Vector2D{-1, -1} || can_jump_to(posA, posB);
    case 2:
    case 4:
      return posB == posA + Vector2D{1, 1} || posB == posA + Vector2D{-1, 1} ||
             can_jump_to(posA, posB);
    default:
      return false;
  }
}

void Dames::play_round(Position pos) {
  if (selected_position.has_value()) {
    move_pawn(pos);
  } else {
    select_pawn(pos);
  }
}

bool Dames::can_jump_to(const Position posA, const Position posB) const {
  uint32_t other_player_symbol = get_current_player().get_id() == 1 ? 2 : 1;
  return grid->is_position_valid(posA) && grid->is_position_valid(posB) &&
         grid->is_empty(posB) && is_on_diagonal(posA, posB, 2) &&
         grid->get_symbol((posA + posB.distance_to(posA) / 2)) ==
             other_player_symbol;
}

bool Dames::is_on_diagonal(const Position posA, const Position posB,
                           int dist) const {
  return posA == posB + Vector2D{dist, dist} ||
         posA == posB + Vector2D{-dist, dist} ||
         posA == posB + Vector2D{dist, -dist} ||
         posA == posB + Vector2D{-dist, -dist};
}

bool Dames::can_move(const Position pos) const {
  uint32_t symbol = grid->get_symbol(pos);
  for (uint32_t row = 0; row < grid->get_rows_number(); row++) {
    for (uint32_t column = 0; column < grid->get_columns_number(); column++) {
      if (is_move_position_valid(pos, Position(row, column))) {
        return true;
      }
    }
  }
  return false;
}

void Dames::select_pawn(const Position pos) {
  grid->set_symbol(pos, grid->get_symbol(pos) + 2);
  selected_position = pos;
}

void Dames::move_pawn(const Position pos) {
  if (can_jump_to(selected_position.value(), pos)) {
    grid->set_symbol((selected_position.value() +
                      pos.distance_to(selected_position.value()) / 2),
                     Grid::EMPTY_SYMBOL);
  }
  grid->set_symbol(pos, grid->get_symbol(selected_position.value()) - 2);
  grid->set_symbol(selected_position.value(), Grid::EMPTY_SYMBOL);
  selected_position.reset();
}

bool Dames::is_over() const {
  return grid->get_symbols_count(1) == 0 || grid->get_symbols_count(2) == 0;
}

bool Dames::is_draw() const { return false; }

uint32_t Dames::get_winner_id() const {
  return grid->get_symbols_count(1) >= grid->get_symbols_count(2) ? 1 : 2;
}

uint32_t Dames::get_next_player_index() const {
  return selected_position.has_value()
             ? current_player_index
             : (current_player_index + 1) % players.size();
}
