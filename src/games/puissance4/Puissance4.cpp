#include "Puissance4.h"
#include "custom_types/save_struct/SaveStruct.h"
#include <iostream>
#include <limits>
#include <memory>
#include <string>

Puissance4::Puissance4(std::vector<Player> &players) : Game(players) {
  initialize_grid();
  game_type = GameType::PUISSANCE4;
}

Puissance4::Puissance4(SaveData &data, std::vector<Player> &players)
    : Game(data, players) {
  game_type = GameType::PUISSANCE4;
}

void Puissance4::initialize_grid() { grid = std::make_unique<Grid>(6, 7); }

bool Puissance4::check_user_move(Position pos) const {
  return grid->is_position_valid(pos) && grid->is_empty(pos) &&
         !grid->is_column_full(pos.get_column());
}

void Puissance4::play_round(Position pos) {
  pos = grid->get_first_empty_position_in_column(pos.get_column());
  grid->set_symbol(pos, get_current_player().get_id());
  if (check_winning_position(pos)) {
    _winner_id = get_current_player().get_id();
  }
}

bool Puissance4::is_over() const { return is_draw() || _winner_id != 0; }

bool Puissance4::is_draw() const { return grid->is_full() && _winner_id == 0; }

bool Puissance4::check_winning_position(const Position &position) const {
  return check_4_aligned(grid->get_row(position.get_row())) ||
         check_4_aligned(grid->get_column(position.get_column())) ||
         check_4_aligned(grid->get_diagonal(position)) ||
         check_4_aligned(grid->get_anti_diagonal(position));
}

bool Puissance4::check_4_aligned(const std::vector<uint32_t> &symbols) const {
  if (symbols.size() < 4) {
    return false;
  }

  uint32_t count = 0;
  uint32_t current_symbol = symbols[0];

  for (uint32_t symbol : symbols) {
    if (symbol == Grid::EMPTY_SYMBOL) {
      count = 0;
      continue;
    }
    if (symbol != current_symbol) {
      current_symbol = symbol;
      count = 1;
      continue;
    }

    count++;
    if (count == 4) {
      return true;
    }
  }
  return false;
}