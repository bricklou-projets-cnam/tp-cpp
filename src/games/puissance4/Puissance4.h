#pragma once

#include "models/game/Game.h"
#include "custom_types/save_struct/SaveStruct.h"
#include <memory>

class Puissance4 : public Game {
  public:
    Puissance4(std::vector<Player> &players);
    Puissance4(SaveData &data, std::vector<Player> &players);
    void initialize_grid() override;
    void play_round(Position pos) override;
    bool is_over() const override;
    bool is_draw() const override;
    bool check_user_move(Position pos) const override;

  private:
    bool check_winning_position(const Position &position) const;
    bool check_4_aligned(const std::vector<uint32_t> &symbols) const;
    uint32_t const _nb_to_win = 4;
};