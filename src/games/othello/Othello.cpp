#include "Othello.h"
#include "custom_types/vector/Vector2D.h"
#include "custom_types/save_struct/SaveStruct.h"
#include <iostream>
#include <limits>
#include <memory>
#include <string>

Othello::Othello(std::vector<Player> &players) : Game(players) {
  initialize_grid();
  game_type = GameType::OTHELLO;
}

Othello::Othello(SaveData &data, std::vector<Player> &players)
    : Game(data, players) {
  game_type = GameType::OTHELLO;
}

void Othello::initialize_grid() {
  grid = std::make_unique<Grid>(8, 8);
  grid->set_symbol(Position(3, 3), 2);
  grid->set_symbol(Position(4, 4), 2);
  grid->set_symbol(Position(3, 4), 1);
  grid->set_symbol(Position(4, 3), 1);
}

bool Othello::check_user_move(Position pos) const {
  return is_position_valid(pos, get_current_player().get_id());
}

void Othello::play_round(Position pos) {
  if (get_valid_positions().empty()) {
    return;
  }
  grid->set_symbol(pos, get_current_player().get_id());
  flip_symbols(pos);
}

bool Othello::is_position_valid(const Position &position,
                                uint32_t symbol) const {
  if (!grid->is_position_valid(position) || !grid->is_empty(position)) {
    return false;
  }

  // Check every direction
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) {
      if (i == 0 && j == 0) {
        continue;
      }
      if (is_position_valid_direction(position, Vector2D{i, j}, symbol)) {
        return true;
      }
    }
  }
  return false;
}

bool Othello::is_position_valid_direction(const Position &position,
                                          const Vector2D &direction,
                                          uint32_t symbol) const {
  Position current_pos = position + direction;

  // Check if the first symbol is different from the given symbol
  if (!grid->is_position_valid(current_pos) || grid->is_empty(current_pos) ||
      grid->get_symbol(current_pos) == symbol) {
    return false;
  }

  // Check if there is a symbol of the same type in the given direction
  current_pos = current_pos + direction;
  while (grid->is_position_valid(current_pos)) {
    if (grid->get_symbol(current_pos) == symbol) {
      return true;
    } else if (grid->get_symbol(current_pos) == Grid::EMPTY_SYMBOL) {
      return false;
    }
    current_pos = current_pos + direction;
  }
  return false;
}

bool Othello::is_position_valid_direction(const Position &position,
                                          const Vector2D &direction) const {
  return is_position_valid_direction(position, direction,
                                     get_current_player().get_id());
}

bool Othello::is_over() const {
  for (const auto &player : players) {
    if (!get_valid_positions(player.get_id()).empty()) {
      return false;
    }
  }
  return true;
}

bool Othello::is_draw() const {
  return grid->get_symbols_count(1) == grid->get_symbols_count(2);
}

uint32_t Othello::get_winner_id() const {
  return grid->get_symbols_count(1) >= grid->get_symbols_count(2) ? 1 : 2;
}

void Othello::flip_symbols(const Position &position) {
  uint32_t symbol = grid->get_symbol(position);
  for (int i = -1; i <= 1; i++) {
    for (int j = -1; j <= 1; j++) {
      if (i == 0 && j == 0) {
        continue;
      }
      if (is_position_valid_direction(position, Vector2D{i, j}, symbol)) {
        flip_symbols_direction(position, Vector2D{i, j});
      }
    }
  }
}

void Othello::flip_symbols_direction(const Position &position,
                                     const Vector2D &direction) {
  Position current_pos = position + direction;
  uint32_t symbol = grid->get_symbol(position);

  while (grid->is_position_valid(current_pos)) {
    if (grid->get_symbol(current_pos) == symbol) {
      return;
    }
    grid->set_symbol(current_pos, symbol);
    current_pos += direction;
  }
}

std::vector<Position> Othello::get_valid_positions() const {
  return get_valid_positions(get_current_player().get_id());
}

std::vector<Position> Othello::get_valid_positions(uint32_t symbol) const {
  std::vector<Position> valid_positions;
  for (uint32_t row = 0; row < grid->get_rows_number(); row++) {
    for (uint32_t column = 0; column < grid->get_columns_number(); column++) {
      if (is_position_valid(Position(row, column), symbol)) {
        valid_positions.push_back(Position(row, column));
      }
    }
  }
  return valid_positions;
}
