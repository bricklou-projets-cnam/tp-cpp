#pragma once

#include "models/game/Game.h"
#include "custom_types/save_struct/SaveStruct.h"
#include <map>
#include <memory>

class Othello : public Game {
  public:
    Othello(std::vector<Player> &players);
    Othello(SaveData &data, std::vector<Player> &players);
    void initialize_grid() override;
    void play_round(Position pos) override;
    bool is_over() const override;
    bool is_draw() const override;
    uint32_t get_winner_id() const override;
    bool check_user_move(Position pos) const override;

  private:
    bool is_position_valid(const Position &position, uint32_t symbol) const;
    bool is_position_valid_direction(const Position &position,
                                     const Vector2D &direction) const;
    bool is_position_valid_direction(const Position &position,
                                     const Vector2D &direction,
                                     uint32_t symbol) const;
    std::vector<Position> get_valid_positions() const;
    std::vector<Position> get_valid_positions(uint32_t symbol) const;
    void flip_symbols(const Position &position);
    void flip_symbols_direction(const Position &position,
                                const Vector2D &direction);
};