#include "Morpion.h"
#include "custom_types/vector/Vector2D.h"
#include <iostream>
#include <limits>
#include <memory>
#include <string>

Morpion::Morpion(std::vector<Player> &players) : Game(players) {
  initialize_grid();
  game_type = GameType::MORPION;
}

Morpion::Morpion(SaveData &data, std::vector<Player> &players)
    : Game(data, players) {
  game_type = GameType::MORPION;
}

void Morpion::initialize_grid() { grid = std::make_unique<Grid>(3, 3); }

bool Morpion::check_user_move(Position pos) const {
  return grid->is_position_valid(pos) && grid->is_empty(pos);
}

void Morpion::play_round(Position pos) {
  grid->set_symbol(pos, get_current_player().get_id());
  if (check_winning_position(pos)) {
    _winner_id = get_current_player().get_id();
  }
}

bool Morpion::is_over() const { return is_draw() || _winner_id != 0; }

bool Morpion::is_draw() const { return grid->is_full() && _winner_id == 0; }

bool Morpion::check_winning_position(const Position &position) const {
  return check_3_aligned(grid->get_row(position.get_row())) ||
         check_3_aligned(grid->get_column(position.get_column())) ||
         check_3_aligned(grid->get_diagonal()) ||
         check_3_aligned(grid->get_anti_diagonal());
}

bool Morpion::check_3_aligned(const std::vector<uint32_t> &symbols) const {
  // check if 3 not empty symbols are aligned
  if (symbols.size() < 3) {
    return false;
  }

  uint32_t count = 0;
  uint32_t current_symbol = symbols[0];

  for (uint32_t symbol : symbols) {
    if (symbol == Grid::EMPTY_SYMBOL) {
      count = 0;
      continue;
    }
    if (symbol != current_symbol) {
      current_symbol = symbol;
      count = 1;
      continue;
    }

    count++;
    if (count == 3) {
      return true;
    }
  }
  return false;
}