#pragma once

#include "custom_types/position/Position.h"
#include "custom_types/vector/Vector2D.h"
#include "models/game/Game.h"
#include "models/grid/Grid.h"
#include "models/player/Player.h"
#include <memory>
#include <string>

class UserInterface {
  public:
    virtual void display_grid(const Grid &grid) const = 0;
    virtual void display_game(const std::unique_ptr<Game> &game) const = 0;
    virtual void display_game_over(const Game &game) const = 0;
    virtual void print_success_message(const std::string &text) const = 0;
    virtual void print_error_message(const std::string &text) const = 0;
    virtual void print_info_message(const std::string &text) const = 0;
    virtual void clear_screen() const = 0;
    virtual void print_score(const std::vector<Player> &players) const = 0;
    virtual void print_menu(const std::string &title,
                            const std::vector<std::string> &items) const = 0;
    virtual char symbol_to_char(const uint32_t &symbol) const = 0;

    virtual uint32_t read_input_uint32(const std::string &message) const = 0;
    virtual std::string read_input_string(const std::string &message) const = 0;
    virtual Position read_input_position(const std::string &message) const = 0;
};