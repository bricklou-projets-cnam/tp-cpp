#include "QtUI.h"
#include "ui/UserInterface.h"
#include "ui/qt/widgets/menu/main/main_menu.h"

#include <QMessageBox>
#include <qdialog.h>
#include <qmessagebox.h>
#include <thread>

QtUI::QtUI(int &argc, char **argv) : _app(argc, argv) {
  _main_window.show();

  _app.exec();
}

QtUI::~QtUI() { _app_thread.join(); }
