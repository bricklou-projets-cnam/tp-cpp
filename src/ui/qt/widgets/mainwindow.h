#pragma once

#include "models/game/Game.h"
#include "custom_types/game_type/GameType.h"
#include "custom_types/save_struct/SaveStruct.h"
#include <QMainWindow>
#include <memory>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void set_content(QWidget *widget);
    void show_main_menu();

  private slots:
    void on_close();
    void on_gameFinished();
    void on_load();

  public slots:
    void play(GameType type);
    void play(SaveData &data);

  private:
    std::unique_ptr<Ui::MainWindow> _ui;
    std::unique_ptr<QWidget> _content;

    std::vector<Player> _players;
};
