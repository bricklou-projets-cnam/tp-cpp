#pragma once

#include "custom_types/game_type/GameType.h"
#include <QWidget>
#include <memory>

class MainWindow;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainMenu;
}
QT_END_NAMESPACE

class MainMenu : public QWidget {
    Q_OBJECT

  public:
    MainMenu(MainWindow *parent);
    ~MainMenu();

  private slots:
    void on_mainMenuHandle();

  signals:
    void gameSeleted(GameType type);

  private:
    std::unique_ptr<Ui::MainMenu> _ui;
};