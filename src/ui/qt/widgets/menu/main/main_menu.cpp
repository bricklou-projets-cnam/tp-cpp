#include "main_menu.h"
#include "./ui_main_menu.h"
#include "custom_types/game_type/GameType.h"
#include "ui/qt/widgets/mainwindow.h"
#include <QLayout>
#include <qobject.h>
#include <qobjectdefs.h>
#include <qpushbutton.h>

MainMenu::MainMenu(MainWindow *parent)
    : QWidget(parent), _ui(new Ui::MainMenu) {
  _ui->setupUi(this);

  // connect EXIT button
  connect(_ui->quitBtn, SIGNAL(clicked()), parent, SLOT(on_close()));
  // connect MORPION button
  connect(_ui->playMorpionBtn, SIGNAL(clicked()), this,
          SLOT(on_mainMenuHandle()));
  // connect PUISSANCE 4 button
  connect(_ui->playPuissance4Btn, SIGNAL(clicked()), this,
          SLOT(on_mainMenuHandle()));
  // connect OTHELLO button
  connect(_ui->playOthelloBtn, SIGNAL(clicked()), this,
          SLOT(on_mainMenuHandle()));
  // connect DAMES button
  connect(_ui->playDamesBt, SIGNAL(clicked()), this, SLOT(on_mainMenuHandle()));

  // Register connections with parent
  connect(this, SIGNAL(gameSeleted(GameType)), this->parent(),
          SLOT(play(GameType)));
}

MainMenu::~MainMenu() {}

void MainMenu::on_mainMenuHandle() {
  QPushButton *obj = qobject_cast<QPushButton *>(sender());

  GameType type = GameType::MORPION;

  if (obj->objectName() == "playMorpionBtn") {
    type = GameType::MORPION;
  } else if (obj->objectName() == "playPuissance4Btn") {
    type = GameType::PUISSANCE4;
  } else if (obj->objectName() == "playOthelloBtn") {
    type = GameType::OTHELLO;
  } else if (obj->objectName() == "playDamesBt") {
    type = GameType::DAMES;
  } else {
    return;
  }

  emit gameSeleted(type);
}