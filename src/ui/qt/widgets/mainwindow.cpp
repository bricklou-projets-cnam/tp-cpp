#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "serializer/Serializer.h"
#include "ui/qt/widgets/game/game_view.h"
#include "ui/qt/widgets/menu/main/main_menu.h"
#include <QDebug>
#include <QFileDialog>
#include <QLayout>
#include <QMessageBox>
#include <iostream>
#include <ostream>
#include <qobjectdefs.h>
#include <utility>
#include <vector>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), _ui(new Ui::MainWindow) {
  _ui->setupUi(this);

  _players = std::vector<Player>{{}, {}};

  show_main_menu();

  connect(_ui->actionExit, SIGNAL(triggered()), this, SLOT(on_close()));
  connect(_ui->actionLoadGame, SIGNAL(triggered()), this, SLOT(on_load()));
}

MainWindow::~MainWindow() {}

void MainWindow::set_content(QWidget *widget) {
  // Remove all connections
  if (this->_content) {
    this->_content->disconnect();
  }

  this->_content.reset(widget);

  // Update the widget content
  if (this->_content) {
    this->_content->setParent(this);
    this->centralWidget()->layout()->addWidget(this->_content.get());
    this->_content->show();
  }
}

void MainWindow::on_close() { QApplication::quit(); }

void MainWindow::play(GameType type) {
  _ui->actionSaveGame->setDisabled(false);

  GameView *game_view = new GameView(type, _players);
  set_content(game_view);

  connect(_ui->actionSaveGame, SIGNAL(triggered()), this->_content.get(),
          SLOT(on_save()));
  connect(this->_content.get(), SIGNAL(gameFinished()), this,
          SLOT(on_gameFinished()));
}

void MainWindow::play(SaveData &data) {
  _ui->actionSaveGame->setDisabled(false);

  GameView *game_view = new GameView(data, _players);
  set_content(game_view);

  connect(_ui->actionSaveGame, SIGNAL(triggered()), this->_content.get(),
          SLOT(on_save()));
  connect(this->_content.get(), SIGNAL(gameFinished()), this,
          SLOT(on_gameFinished()));
}

void MainWindow::on_gameFinished() { this->show_main_menu(); }

void MainWindow::show_main_menu() {
  _ui->actionSaveGame->setDisabled(true);

  set_content(new MainMenu(this));
}

void MainWindow::on_load() {
  QFileDialog fileDialog(this);
  fileDialog.setFileMode(QFileDialog::ExistingFile);
  fileDialog.setWindowTitle("Sauvegarder la partie");
  fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
  fileDialog.setDefaultSuffix("gsave");
  fileDialog.setNameFilter(tr("Game Saves (*.gsave)"));

  if (!fileDialog.exec() || fileDialog.selectedFiles().empty())
    return;

  try {
    SaveData data =
        Serializer::load(fileDialog.selectedFiles().first().toStdString());
    _players = data.players;
    emit play(data);
  } catch (std::runtime_error &e) {
    qDebug() << "Error while loading the game: " << e.what();
    QMessageBox msgBox;
    msgBox.critical(this, "Erreur",
                    "Une erreur est survenue lors du chargement de la partie.");
  }
}