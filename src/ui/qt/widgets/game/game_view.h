#pragma once

#include "custom_types/game_type/GameType.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "ui/UserInterface.h"
#include <QWidget>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
class gameView;
}
QT_END_NAMESPACE

class GameView : public QWidget {
    Q_OBJECT

  public:
    GameView(GameType type, std::vector<Player> &players,
             QWidget *parent = nullptr);
    GameView(SaveData &data, std::vector<Player> &players,
             QWidget *parent = nullptr);
    ~GameView();
    void display_grid(const Grid &grid) const;

  signals:
    void gameFinished();

  public slots:
    void on_save();

  private slots:
    void on_btnClicked();
    void on_gameWinned(int playerId);

  private:
    std::unique_ptr<Ui::gameView> _ui;
    std::unique_ptr<Game> _game;

    void show_current_player();
    bool is_playing() const { return !!_game; }
};