#include "game_view.h"
#include "./ui_game_view.h"
#include "custom_types/position/Position.h"
#include "custom_types/vector/Vector2D.h"
#include "models/game/Game.h"
#include "factories/game_factory/GameFactory.h"
#include "custom_types/game_type/GameType.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "serializer/Serializer.h"
#include "ui/UserInterface.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <memory>
#include <vector>

GameView::GameView(GameType type, std::vector<Player> &players, QWidget *parent)
    : QWidget(parent), _ui(new Ui::gameView) {
  _ui->setupUi(this);

  _game = GameFactory::create_game(type, players);

  display_grid(_game->get_grid());
  show_current_player();
}

GameView::GameView(SaveData &data, std::vector<Player> &players,
                   QWidget *parent)
    : QWidget(parent), _ui(new Ui::gameView) {
  _ui->setupUi(this);

  _game = GameFactory::create_game(data, players);

  display_grid(_game->get_grid());
  show_current_player();
}

GameView::~GameView() {}

void GameView::display_grid(const Grid &grid) const {
  QGridLayout *layout =
      dynamic_cast<QGridLayout *>(_ui->gridRenderer->layout());

  // Clear layout childrens
  QLayoutItem *wItem;
  while ((wItem = layout->takeAt(0)) != 0)
    delete wItem;

  for (uint32_t row = 0; row < grid.get_rows_number(); row++) {
    for (uint32_t col = 0; col < grid.get_columns_number(); col++) {
      auto symb = grid.get_symbol({row, col});
      auto btn = new QPushButton(QString::number(symb));
      btn->setFixedSize(QSize(100, 100));
      btn->setProperty("row", row);
      btn->setProperty("col", col);

      if (!_game->check_user_move({row, col})) {
        btn->setDisabled(true);
      }

      connect(btn, SIGNAL(clicked()), this, SLOT(on_btnClicked()));

      if (symb == 1) {
        btn->setStyleSheet(
            "QPushButton { background-color: #E61919; color: black; }\n"
            "QPushButton:enabled { background-color: #F07575; color: black; "
            "}\n");
      } else if (symb == 2) {
        btn->setStyleSheet(
            "QPushButton { background-color: #1980E6; color: black; }\n"
            "QPushButton:enabled { background-color: #75B3F0; color: black: "
            "}\n");
      }

      layout->addWidget(btn, row, col);
    }
  }
}

void GameView::on_btnClicked() {
  auto object = qobject_cast<QPushButton *>(sender());

  Position pos;
  pos.set_column(object->property("col").toUInt());
  pos.set_row(object->property("row").toUInt());

  _game->play_round(pos);

  if (!_game->is_over()) {
    _game->switch_player();
    display_grid(_game->get_grid());
    show_current_player();
  } else {
    display_grid(_game->get_grid());

    _game->increment_winner_score();

    auto p = _game->get_winner_id();
    emit on_gameWinned(p);
  }
}

void GameView::on_gameWinned(int playerId) {
  QMessageBox msgBox;
  QString msg = _game->is_draw()
                    ? QString("Match nul !")
                    : QString("Le joueur %1 a gagné !").arg(playerId);
  msgBox.setText(msg);
  msgBox.setWindowTitle("Partie terminée");

  auto players = _game->get_players();
  QString scoreMsg = QString("Résultats:\n-Joueur 1: %1\n-Joueur 2: %2")
                         .arg(players[0].get_score())
                         .arg(players[1].get_score());

  msgBox.setDetailedText(scoreMsg);
  msgBox.exec();

  emit gameFinished();
}

void GameView::show_current_player() {
  auto current = _game->get_current_player().get_id();
  _ui->gridRenderer->setTitle(
      QString("Grille de jeu (joueur %1 joue)").arg(current));

  if (current == 1) {
    _ui->gridRenderer->setStyleSheet("QGroupBox { color: #E61919; }\n");
  } else if (current == 2) {
    _ui->gridRenderer->setStyleSheet("QGroupBox { color: #1980E6; }\n");
  } else {
    _ui->gridRenderer->setStyleSheet("QGroupBox { color: inherit; }\n");
  }
}

void GameView::on_save() {
  qDebug() << "Saving game...";

  if (!is_playing())
    return;

  QFileDialog fileDialog;
  fileDialog.setFileMode(QFileDialog::AnyFile);
  fileDialog.setWindowTitle("Ouvrir une partie");
  fileDialog.setAcceptMode(QFileDialog::AcceptSave);
  fileDialog.setNameFilter(tr("Game Save (*.gsave)"));
  fileDialog.setDefaultSuffix("gsave");

  if (!fileDialog.exec() || fileDialog.selectedFiles().empty())
    return;

  try {
    Serializer::save(_game, fileDialog.selectedFiles().first().toStdString());
  } catch (std::runtime_error &e) {
    qDebug() << "Error while saving game: " << e.what();
    QMessageBox msgBox;
    msgBox.critical(
        this, "Erreur",
        "Une erreur est survenue lors de la sauvegarde de la partie.");
  }

  qDebug() << "Game saved.";
}