#pragma once

#include "widgets/mainwindow.h"
#include <QApplication>
#include <thread>

class QtUI {
  public:
    QtUI(int &argc, char *argv[]);
    ~QtUI();

  private:
    QApplication _app;
    MainWindow _main_window;

    std::thread _app_thread;
};