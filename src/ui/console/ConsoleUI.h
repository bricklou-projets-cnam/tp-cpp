#pragma once

#include "color.h"
#include "models/game/Game.h"
#include "models/grid/Grid.h"
#include "models/player/Player.h"
#include "ui/UserInterface.h"
#include <memory>

class ConsoleUI : public UserInterface {
  public:
    ConsoleUI();
    ~ConsoleUI();

    void show_main_menu() {}

    void display_grid(const Grid &grid) const override;
    void display_game(const std::unique_ptr<Game> &game) const override;
    void display_game_over(const Game &game) const override;
    void print_success_message(const std::string &text) const override;
    void print_error_message(const std::string &text) const override;
    void print_info_message(const std::string &text) const override;
    void clear_screen() const override;
    void print_score(const std::vector<Player> &players) const override;
    void print_menu(const std::string &title,
                    const std::vector<std::string> &items) const override;
    char symbol_to_char(const uint32_t &symbol) const override;

    uint32_t read_input_uint32(const std::string &message) const override;
    std::string read_input_string(const std::string &message) const override;
    Position read_input_position(const std::string &message) const override;

  private:
    void print_message(const std::string &text,
                       const std::string &color = color::reset) const;
    void display_grid_row(const Grid &grid, uint32_t row) const;
    void display_grid_header(const Grid &grid) const;
    void display_grid_separator(const Grid &grid) const;
    bool is_uint32(const std::string &str) const;
    uint32_t str_to_uint32(const std::string &str) const;
};