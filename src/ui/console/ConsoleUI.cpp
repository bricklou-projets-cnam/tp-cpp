#include "ui/console/ConsoleUI.h"
#include "ConsoleUI.h"
#include "utils.h"
#include <iostream>
#include <limits>
#include <memory>
#include <ostream>
#include <regex>
#include <string>

ConsoleUI::ConsoleUI() : UserInterface() {}

ConsoleUI::~ConsoleUI() {}

void ConsoleUI::display_grid(const Grid &grid) const {
  std::cout << _utils::clearScreen();
  display_grid_header(grid);
  display_grid_separator(grid);
  for (uint32_t row = 0; row < grid.get_rows_number(); row++) {
    display_grid_row(grid, row);
    display_grid_separator(grid);
  }
}

void ConsoleUI::display_grid_header(const Grid &grid) const {
  std::cout << "     ";
  for (uint32_t column = 0; column < grid.get_columns_number(); column++) {
    std::cout << " " << column + 1 << "  ";
  }
  std::cout << std::endl;
}

void ConsoleUI::display_grid_separator(const Grid &grid) const {
  std::cout << "    ";
  for (uint32_t column = 0; column < grid.get_columns_number(); column++) {
    std::cout << "----";
  }
  std::cout << "-" << std::endl;
}

void ConsoleUI::display_grid_row(const Grid &grid, uint32_t row) const {
  std::cout << "  " << row + 1 << " ";
  for (uint32_t column = 0; column < grid.get_columns_number(); column++) {
    if (column == 0) {
      std::cout << "| ";
    }
    uint32_t sym = grid.get_symbol(Position(row, column));
    std::cout << symbol_to_char(sym) << " | ";
  }
  std::cout << std::endl;
}

char ConsoleUI::symbol_to_char(const uint32_t &symbol) const {
  switch (symbol) {
    case Grid::EMPTY_SYMBOL:
      return ' ';
    case 1:
      return 'x';
    case 2:
      return 'o';
    case 3:
      return 'X';
    case 4:
      return 'O';
    default:
      return '?';
  }
}

void ConsoleUI::print_success_message(const std::string &text) const {
  print_message("✅ " + text, color::green);
}

void ConsoleUI::print_error_message(const std::string &text) const {
  print_message("❌ " + text, color::red);
  std::cout << _utils::cursorUp(1) << _utils::cursorHorizontalAbsolute(1);
}

void ConsoleUI::print_info_message(const std::string &text) const {
  print_message("ℹ️  " + text, color::blue);
}

void ConsoleUI::print_message(const std::string &text,
                              const std::string &color) const {
  std::cout << _utils::cursorHorizontalAbsolute(1);
  std::cout << _utils::clearLine(_utils::ENTIRE_LINE);
  std::cout << color << text << color::reset;
}

void ConsoleUI::clear_screen() const { std::cout << _utils::clearScreen(); }

void ConsoleUI::print_score(const std::vector<Player> &players) const {
  std::cout << "Score:" << std::endl;
  for (auto &player : players) {
    std::cout << "  Player " << player.get_id() << ": " << player.get_score()
              << std::endl;
  }
}

void ConsoleUI::print_menu(const std::string &title,
                           const std::vector<std::string> &options) const {
  clear_screen();
  std::cout << "🎮 " << title << std::endl;
  for (uint32_t i = 0; i < options.size(); i++) {
    std::cout << "  " << i + 1 << ". " << options[i] << std::endl;
  }
}

uint32_t ConsoleUI::read_input_uint32(const std::string &message) const {
  std::string input;
  uint32_t input_uint32;
  bool valid_input = false;
  while (!valid_input) {
    input = read_input_string(message);
    try {
      input_uint32 = str_to_uint32(input);
      valid_input = true;
    } catch (std::invalid_argument &e) {
      print_error_message("Invalid input");
    } catch (std::out_of_range &e) {
      print_error_message("Input out of range");
    }
  }
  return input_uint32;
}

std::string ConsoleUI::read_input_string(const std::string &message) const {
  print_message("❓ " + message + ": ");
  std::string input;
  std::getline(std::cin, input);
  return input;
}

Position ConsoleUI::read_input_position(const std::string &message) const {
  Position position;
  position.set_row(read_input_uint32(message + " (row ↕️)"));
  position.set_column(read_input_uint32(message + " (column ↔️)"));
  std::cout << _utils::clearLine(_utils::ENTIRE_LINE) << _utils::cursorUp(1)
            << _utils::cursorHorizontalAbsolute(1);
  return position - Vector2D{1, 1};
}

bool ConsoleUI::is_uint32(const std::string &str) const {
  std::regex regex("^(0|([1-9][0-9]*))$");
  return !str.empty() && std::regex_match(str, regex);
}

uint32_t ConsoleUI::str_to_uint32(const std::string &str) const {
  if (!is_uint32(str)) {
    throw std::invalid_argument("Invalid input");
  }
  return std::stoul(str);
}

void ConsoleUI::display_game(const std::unique_ptr<Game> &game) const {
  clear_screen();
  display_grid(game->get_grid());
}

void ConsoleUI::display_game_over(const Game &game) const {
  if (game.is_draw()) {
    print_info_message("It's a draw.\n");
  } else {
    uint32_t winner_id = game.get_winner_id();
    print_success_message("Player " + std::to_string(winner_id) + " won!\n");
  }
  print_score(game.get_players());
}
