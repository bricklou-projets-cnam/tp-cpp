#pragma once

#include <string>

namespace _utils {
  enum ClearMove : int {
    CURSOR_TO_END_OF_SCREEN = 0,
    CURSOR_TO_START_OF_SCREEN = 1,
    ENTIRE_LINE = 2,
  };

  inline std::string clearLine(ClearMove mode) {
    if (mode < 1 && mode > 3) {
      throw "Invalid mode provided in clearLn";
    }
    return "\033[" + std::to_string(mode) + "K";
  }

  inline std::string cursorUp(int n) {
    return "\033[" + std::to_string(n) + "A";
  }

  inline std::string cursorDown(int n) {
    return "\033[" + std::to_string(n) + "B";
  }

  inline std::string cursorLeft(int n) {
    return "\033[" + std::to_string(n) + "D";
  }

  inline std::string cursorRight(int n) {
    return "\033[" + std::to_string(n) + "C";
  }

  inline std::string cursorHorizontalAbsolute(int n) {
    return "\033[" + std::to_string(n) + "G";
  }

  inline std::string clearScreen() { return "\033[2J\033[1;1H"; }
} // namespace _utils