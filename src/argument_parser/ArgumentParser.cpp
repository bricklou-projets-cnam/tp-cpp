#include "ArgumentParser.h"
#include <algorithm>
#include <stdexcept>

ArgumentParser::ArgumentParser(int argc, char **argv)
    : _args(argv + 1, argv + argc) {
  if (argc > 64) {
    throw std::runtime_error("Too many arguments");
  }
}

bool ArgumentParser::has_argument(const std::string &flag) const {
  return std::find(_args.begin(), _args.end(), flag) != _args.end();
}

bool ArgumentParser::has_flag(const std::string &flag) const {
  return has_argument(std::string("--") + flag);
}
