#pragma once

#include <string_view>
#include <vector>

class ArgumentParser {
  public:
    ArgumentParser(int argc, char **argv);
    bool has_flag(const std::string &flag) const;
    bool has_argument(const std::string &flag) const;

  private:
    std::vector<std::string_view> _args;
};
