#pragma once

#include "app/App.h"
#include "custom_types/position/Position.h"
#include "models/game/Game.h"
#include "ui/UserInterface.h"
#include <vector>

class ConsoleApp : public App {
  public:
    ConsoleApp(int &argc, char *argv[]);
    int run() override;

  private:
    std::unique_ptr<UserInterface> _ui;
    std::unique_ptr<Game> _game;
    void setup();
    void main_loop();
    void game_over();
    void create_game(GameType game_type);
    bool load_game();
    Position get_user_move() const;
    bool check_user_move(Position pos) const;
    uint32_t get_game_choice() const;
    bool check_game_choice(uint32_t game_choice) const;
    std::string get_save_file_path() const;
    bool check_save_file_path(std::string save_file_path) const;
    std::vector<Player> _players;

    static void close(int signal);
    static std::unique_ptr<ConsoleApp> _instance;

    // +2 for the load and quit options
    static const uint32_t OPTIONS_COUNT =
        static_cast<uint32_t>(GameType::GAME_COUNT) + 2;
    // LOAD and QUIT constants
    static const uint32_t LOAD = OPTIONS_COUNT - 1;
    static const uint32_t QUIT = OPTIONS_COUNT;
};
