#include "ConsoleApp.h"
#include "custom_types/game_type/GameType.h"
#include "factories/game_factory/GameFactory.h"
#include "serializer/Serializer.h"
#include "ui/console/ConsoleUI.h"
#include <csignal>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <memory>
#include <ostream>
#include <string>

std::unique_ptr<ConsoleApp> ConsoleApp::_instance = nullptr;

ConsoleApp::ConsoleApp(int &argc, char *argv[]) : App(argc, argv) {
  _players = {{}, {}};
  _ui = std::make_unique<ConsoleUI>();
  if (!_instance) {
    _instance = std::unique_ptr<ConsoleApp>(this);
  }
  setup();
}

void ConsoleApp::setup() { std::signal(SIGINT, &ConsoleApp::close); }

int ConsoleApp::run() {
  uint32_t choice;
  while ((choice = get_game_choice()) != QUIT) {
    switch (choice) {
      case LOAD:
        if (!load_game()) {
          continue;
        }
        break;
      default:
        create_game(static_cast<GameType>(choice));
        break;
    }
    main_loop();
    game_over();
  }
  return 0;
}

void ConsoleApp::main_loop() {
  while (!_game->is_over()) {
    _ui->display_game(_game);
    Position pos = get_user_move();
    _game->play_round(pos);
    _game->switch_player();
  }
}

void ConsoleApp::game_over() {
  _ui->display_game(_game);
  _game->increment_winner_score();
  _ui->display_game_over(*_game);
  _ui->read_input_string("Press enter to continue");
}

void ConsoleApp::create_game(GameType game_type) {
  _game = GameFactory::create_game(game_type, _players);
}

bool ConsoleApp::load_game() {
  std::string path = get_save_file_path();
  try {
    SaveData data = Serializer::load(path);
    _game = GameFactory::create_game(data, _players);
    return true;
  } catch (std::runtime_error &e) {
    _ui->print_error_message("Error while loading the game: " +
                             std::string(e.what()));
    _ui->read_input_string("Press enter to continue");
    return false;
  }
}

bool ConsoleApp::check_game_choice(uint32_t game_choice) const {
  if (game_choice <= 0 || game_choice > OPTIONS_COUNT) {
    _ui->print_error_message("Invalid choice, try again");
    return false;
  }
  return true;
}

uint32_t ConsoleApp::get_game_choice() const {
  _ui->print_menu("Welcome to the game",
                  {"Play Morpion", "Play Puissance 4", "Play Othello",
                   "Play Dames", "Load game", "Quit"});
  uint32_t choice;
  do {
    choice = _ui->read_input_uint32("Enter your choice (1-" +
                                    std::to_string(OPTIONS_COUNT) + ")");
  } while (!check_game_choice(choice));
  return choice;
}

Position ConsoleApp::get_user_move() const {
  Position pos;
  uint32_t player_id = _game->get_current_player().get_id();
  do {
    if (_game->get_game_type() == GameType::PUISSANCE4) {
      uint32_t col = _ui->read_input_uint32(
          "Player " + std::to_string(player_id) + " (" +
          _ui->symbol_to_char(player_id) + ")" + ", enter a column");
      pos = Position(0, col - 1);
      continue;
    }
    pos = _ui->read_input_position("Player " + std::to_string(player_id) +
                                   " (" + _ui->symbol_to_char(player_id) + ")" +
                                   ", enter a position");
  } while (!check_user_move(pos));
  return pos;
}

bool ConsoleApp::check_user_move(Position pos) const {
  if (!_game->check_user_move(pos)) {
    _ui->print_error_message("Invalid move, try again");
    return false;
  }
  return true;
}

std::string ConsoleApp::get_save_file_path() const {
  std::string path;
  do {
    path = _ui->read_input_string("Enter the save file path");
  } while (!check_save_file_path(path));
  return path;
}

bool ConsoleApp::check_save_file_path(std::string save_file_path) const {
  if (!std::filesystem::exists(save_file_path)) {
    _ui->print_error_message("Invalid path, try again");
    return false;
  }
  return true;
}

void ConsoleApp::close(int signal) {
  if (!ConsoleApp::_instance || !ConsoleApp::_instance->_game) {
    return std::exit(0);
  }

  std::string filename = "./saves/game.gsave";
  try {
    Serializer::save(ConsoleApp::_instance->_game, filename);
  } catch (std::runtime_error &e) {
    ConsoleApp::_instance->_ui->print_error_message(
        "Failed to save game: " + std::string(e.what()) + "\n\n");
    return std::exit(1);
  }
  ConsoleApp::_instance->_ui->print_success_message("Game saved to " +
                                                    filename + "\n");
  std::exit(0);
}
