#pragma once

class App {
  public:
    App(int &argc, char *argv[]) {}
    virtual int run() = 0;
};
