#include "QtApp.h"
#include "app/App.h"
#include "ui/qt/widgets/menu/main/main_menu.h"
#include <memory>
#include <qapplication.h>

QtApp::QtApp(int &argc, char *argv[])
    : App(argc, argv), QApplication(argc, argv) {
  _main_window = std::make_unique<MainWindow>();
  _main_window->show_main_menu();
}

int QtApp::run() {
  _main_window->show();
  return this->exec();
}
