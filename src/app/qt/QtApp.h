#include "app/App.h"
#include "ui/qt/widgets/mainwindow.h"
#include <memory>
#include <qapplication.h>

class QtApp : public App, private QApplication {

  public:
    QtApp(int &argc, char *argv[]);
    int run() override;

  private:
    std::unique_ptr<MainWindow> _main_window;
};
