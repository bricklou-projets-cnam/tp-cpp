#include "Player.h"
#include <cstdint>

uint32_t Player::ID = 0;

Player::Player() {
  _id = ++ID;
  _score = 0;
}

std::ofstream &operator<<(std::ofstream &out, const Player &player) {
  out.write((char *)&player._id, sizeof(uint32_t));
  out.write((char *)&player._score, sizeof(uint32_t));

  return out;
}
std::ifstream &operator>>(std::ifstream &in, Player &player) {
  in.read((char *)&player._id, sizeof(uint32_t));
  in.read((char *)&player._score, sizeof(uint32_t));

  return in;
}