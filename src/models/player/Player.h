#pragma once

#include <cstdint>
#include <fstream>

class GameLogic;

class Player {
  public:
    Player();
    virtual ~Player() {}
    uint32_t get_id() const { return _id; }
    uint32_t get_score() const { return _score; }
    void increment_score() { _score++; }

    friend std::ofstream &operator<<(std::ofstream &out, const Player &grid);
    friend std::ifstream &operator>>(std::ifstream &in, Player &grid);

  private:
    uint32_t _id;
    static uint32_t ID;
    uint32_t _score;
};

std::ofstream &operator<<(std::ofstream &out, const Player &grid);
std::ifstream &operator>>(std::ifstream &in, Player &grid);