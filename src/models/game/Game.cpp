#include "Game.h"
#include "serializer/Serializer.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "models/grid/Grid.h"
#include "ui/UserInterface.h"
#include <algorithm>
#include <iostream>
#include <memory>
#include <string>

Game::Game(std::vector<Player> &players) : players(players) {}

Game::Game(SaveData &data, std::vector<Player> &players) : players(players) {
  this->grid = std::unique_ptr<Grid>(new Grid(data.grid));
  this->current_player_index = data.current_player_index;
}

uint32_t Game::get_next_player_index() const {
  return (current_player_index + 1) % players.size();
}

void Game::switch_player() { current_player_index = get_next_player_index(); }

void Game::increment_winner_score() {
  if (_winner_id)
    get_player(_winner_id).increment_score();
}