#pragma once

#include "custom_types/game_type/GameType.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "models/grid/Grid.h"
#include "models/player/Player.h"

#include <cstdint>
#include <memory>
#include <vector>

class Game {
  public:
    Game(std::vector<Player> &players);
    Game(SaveData &data, std::vector<Player> &players);

    GameType get_game_type() const { return game_type; }
    uint32_t get_current_player_index() const { return current_player_index; }
    Player &get_current_player() const { return players[current_player_index]; }
    Player &get_player(uint32_t id) const { return players[id - 1]; }
    std::vector<Player> &get_players() const { return players; }
    Grid &get_grid() const { return *grid; }

    virtual uint32_t get_winner_id() const { return _winner_id; }
    virtual uint32_t get_next_player_index() const;
    virtual void switch_player();
    virtual void increment_winner_score();
    virtual void initialize_grid() = 0;
    virtual void play_round(Position pos) = 0;
    virtual bool is_over() const = 0;
    virtual bool is_draw() const = 0;
    virtual bool check_user_move(Position pos) const = 0;

  protected:
    std::unique_ptr<Grid> grid;
    std::vector<Player> &players;
    uint32_t current_player_index = 0;
    uint32_t _winner_id = 0;
    GameType game_type;
};
