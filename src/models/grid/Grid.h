#pragma once

#include "custom_types/position/Position.h"
#include <cstdint>
#include <fstream>
#include <optional>
#include <vector>

class Grid {
  public:
    Grid();
    Grid(const uint32_t rows, const uint32_t columns);
    uint32_t get_rows_number() const { return rows; }
    uint32_t get_columns_number() const { return columns; }

    uint32_t get_symbol(const Position &position) const;
    void set_symbol(const Position &position, uint32_t symbol);

    bool is_position_valid(const Position &position) const;
    bool is_full() const;
    bool is_row_full(uint32_t row) const;
    bool is_column_full(uint32_t column) const;
    bool is_empty(const Position &position) const;
    Position get_first_empty_position_in_column(uint32_t column) const;

    std::vector<uint32_t> get_row(uint32_t row) const;
    std::vector<uint32_t> get_column(uint32_t column) const;
    std::vector<uint32_t> get_diagonal(const Position &position) const;
    std::vector<uint32_t> get_anti_diagonal(const Position &position) const;
    std::vector<uint32_t> get_diagonal() const;
    std::vector<uint32_t> get_anti_diagonal() const;
    uint32_t get_symbols_count(uint32_t symbol) const;
    static const uint32_t EMPTY_SYMBOL = 0;

    friend std::ofstream &operator<<(std::ofstream &out, const Grid &grid);
    friend std::ifstream &operator>>(std::ifstream &in, Grid &grid);

  protected:
    const uint32_t rows;
    const uint32_t columns;
    std::vector<std::vector<uint32_t>> _grid{};
};

std::ofstream &operator<<(std::ofstream &out, const Grid &grid);
std::ifstream &operator>>(std::ifstream &in, Grid &grid);