#include "Grid.h"
#include "custom_types/vector/vector_serialiser.h"
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <stdexcept>

Grid::Grid() : Grid({3, 3}) {}

Grid::Grid(const uint32_t rows, const uint32_t columns)
    : rows(rows), columns(columns) {
  _grid.resize(rows);
  for (uint32_t row = 0; row < rows; row++) {
    _grid[row].resize(columns);
    for (uint32_t column = 0; column < columns; column++) {
      _grid[row][column] = 0;
    }
  }
}

void Grid::set_symbol(const Position &position, uint32_t symbol) {
  if (!is_position_valid(position)) {
    throw std::invalid_argument("Invalid grid coordinates (set_symbol)");
  }
  _grid[position.get_row()][position.get_column()] = symbol;
}

bool Grid::is_position_valid(const Position &position) const {
  return (rows > position.get_row()) && (columns > position.get_column());
}

uint32_t Grid::get_symbol(const Position &position) const {
  if (!is_position_valid(position)) {
    throw std::invalid_argument("Invalid grid coordinates (get_symbol)");
  }
  return _grid[position.get_row()][position.get_column()];
}

bool Grid::is_full() const {
  for (uint32_t row = 0; row < rows; row++) {
    if (!is_row_full(row)) {
      return false;
    }
  }
  return true;
}

bool Grid::is_row_full(uint32_t row) const {
  for (uint32_t column = 0; column < columns; column++) {
    if (is_empty(Position(row, column))) {
      return false;
    }
  }
  return true;
}

bool Grid::is_column_full(uint32_t column) const {
  for (uint32_t row = 0; row < rows; row++) {
    if (is_empty(Position(row, column))) {
      return false;
    }
  }
  return true;
}

bool Grid::is_empty(const Position &position) const {
  return get_symbol(position) == Grid::EMPTY_SYMBOL;
}

Position Grid::get_first_empty_position_in_column(uint32_t column) const {
  for (uint32_t row = rows - 1; row >= 0; row--) {
    Position position(row, column);
    if (is_empty(position)) {
      return position;
    }
  }
  return Position();
}

std::vector<uint32_t> Grid::get_row(uint32_t row) const {
  std::vector<uint32_t> result;
  for (int column = 0; column < columns; column++) {
    result.push_back(get_symbol(Position(row, column)));
  }
  return result;
}

std::vector<uint32_t> Grid::get_column(uint32_t column) const {
  std::vector<uint32_t> result;
  for (int row = 0; row < rows; row++) {
    result.push_back(get_symbol(Position(row, column)));
  }
  return result;
}

std::vector<uint32_t> Grid::get_diagonal(const Position &position) const {
  std::vector<uint32_t> result;
  uint32_t row = position.get_row();
  uint32_t column = position.get_column();

  // Go to the top left corner of the diagonal
  while (row > 0 && column > 0) {
    row--;
    column--;
  }

  // Go to the bottom right corner of the diagonal
  while (row < rows && column < columns) {
    result.push_back(get_symbol(Position(row, column)));
    row++;
    column++;
  }
  return result;
}

std::vector<uint32_t> Grid::get_anti_diagonal(const Position &position) const {
  std::vector<uint32_t> result;
  uint32_t row = position.get_row();
  uint32_t column = position.get_column();

  // Go to the bottom left corner of the diagonal
  while (row < rows - 1 && column > 0) {
    row++;
    column--;
  }

  // Go to the top right corner of the diagonal
  while (row >= 0 && column < columns) {
    result.push_back(get_symbol(Position(row, column)));
    if (row == 0) {
      break;
    }
    row--;
    column++;
  }
  return result;
}

std::ofstream &operator<<(std::ofstream &out, const Grid &grid) {
  out.write((char *)&grid.rows, sizeof(uint32_t));
  out.write((char *)&grid.columns, sizeof(uint32_t));
  out << grid._grid;

  return out;
}
std::ifstream &operator>>(std::ifstream &in, Grid &grid) {
  in.read((char *)&grid.rows, sizeof(uint32_t));
  in.read((char *)&grid.columns, sizeof(uint32_t));
  in >> grid._grid;

  return in;
}

std::vector<uint32_t> Grid::get_diagonal() const {
  return get_diagonal(Position());
}

std::vector<uint32_t> Grid::get_anti_diagonal() const {
  return get_anti_diagonal(Position(rows - 1, 0));
}

uint32_t Grid::get_symbols_count(uint32_t symbol) const {
  uint32_t count = 0;
  for (uint32_t row = 0; row < rows; row++) {
    for (uint32_t column = 0; column < columns; column++) {
      if (get_symbol(Position(row, column)) == symbol) {
        count++;
      }
    }
  }
  return count;
}