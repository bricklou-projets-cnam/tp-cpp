#pragma once

#include "app/App.h"
#include <memory>

class AppFactory {
  public:
    static std::unique_ptr<App> create_app(int &argc, char *argv[]);
};
