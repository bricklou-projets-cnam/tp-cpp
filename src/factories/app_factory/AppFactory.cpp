#include "AppFactory.h"
#include "app/console/ConsoleApp.h"
#include "app/qt/QtApp.h"
#include "argument_parser/ArgumentParser.h"
#include <stdexcept>

std::unique_ptr<App> AppFactory::create_app(int &argc, char *argv[]) {
  ArgumentParser parser(argc, argv);
  if (parser.has_flag("no-gui")) {
    return std::make_unique<ConsoleApp>(argc, argv);
  }
  return std::make_unique<QtApp>(argc, argv);
}