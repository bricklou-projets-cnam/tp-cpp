#pragma once

#include "custom_types/game_type/GameType.h"
#include "models/game/Game.h"
#include "custom_types/save_struct/SaveStruct.h"

class GameFactory {
  public:
    static std::unique_ptr<Game> create_game(const GameType game_type,
                                             std::vector<Player> &players);
    static std::unique_ptr<Game> create_game(SaveData &data,
                                             std::vector<Player> &players);
};
