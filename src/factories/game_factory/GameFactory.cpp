#include "GameFactory.h"
#include "custom_types/save_struct/SaveStruct.h"
#include "games/dames/Dames.h"
#include "games/morpion/Morpion.h"
#include "games/othello/Othello.h"
#include "games/puissance4/Puissance4.h"
#include "ui/UserInterface.h"
#include <memory>
#include <stdexcept>

std::unique_ptr<Game> GameFactory::create_game(const GameType game_type,
                                               std::vector<Player> &players) {
  switch (game_type) {
    case GameType::MORPION:
      return std::make_unique<Morpion>(players);
    case GameType::PUISSANCE4:
      return std::make_unique<Puissance4>(players);
    case GameType::OTHELLO:
      return std::make_unique<Othello>(players);
    case GameType::DAMES:
      return std::make_unique<Dames>(players);
    default:
      throw std::invalid_argument("Invalid game type");
  }
}

std::unique_ptr<Game> GameFactory::create_game(SaveData &data,
                                               std::vector<Player> &players) {
  switch (data.type) {
    case GameType::MORPION:
      return std::make_unique<Morpion>(data, players);
    case GameType::PUISSANCE4:
      return std::make_unique<Puissance4>(data, players);
    case GameType::OTHELLO:
      return std::make_unique<Othello>(data, players);
    default:
      throw std::invalid_argument("Invalid game type");
  }
}