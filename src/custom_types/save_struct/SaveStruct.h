#pragma once

#include "custom_types/game_type/GameType.h"
#include "models/grid/Grid.h"
#include "models/player/Player.h"
#include <array>
#include <vector>

struct SaveData {
    GameType type;
    std::vector<Player> players;
    Grid grid;
    uint32_t current_player_index;
};

std::ofstream &operator<<(std::ofstream &out, const SaveData &data);
std::ifstream &operator>>(std::ifstream &in, SaveData &data);