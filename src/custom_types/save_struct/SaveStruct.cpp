#include "SaveStruct.h"
#include "custom_types/vector/vector_serialiser.h"
#include "custom_types/game_type/GameType.h"
#include <cstdint>
#include <fstream>

std::ofstream &operator<<(std::ofstream &out, const SaveData &data) {
  out.write((char *)&data.type, sizeof(GameType));
  out.write((char *)&data.current_player_index, sizeof(uint32_t));
  out << data.players;
  out << data.grid;

  return out;
}

std::ifstream &operator>>(std::ifstream &in, SaveData &data) {
  in.read((char *)&data.type, sizeof(GameType));
  in.read((char *)&data.current_player_index, sizeof(uint32_t));

  in >> data.players;
  in >> data.grid;

  return in;
}