#pragma once

#include "custom_types/vector/Vector2D.h"
#include <cstdint>

class Position {
  public:
    Position();
    Position(uint32_t x, uint32_t y);
    int get_row() const { return row; }
    int get_column() const { return column; }
    void set_row(uint32_t row) { this->row = row; }
    void set_column(uint32_t column) { this->column = column; }

    bool operator==(const Position &other) const;
    bool operator!=(const Position &other) const;
    Position &operator=(const Position &other);
    Position operator+(const Vector2D &other) const;
    Position operator-(const Vector2D &other) const;
    Position operator+=(const Vector2D &other);
    Position operator-=(const Vector2D &other);
    Vector2D distance_to(const Position &other) const;

  protected:
    uint32_t row;
    uint32_t column;
};