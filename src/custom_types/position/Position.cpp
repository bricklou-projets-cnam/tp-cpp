#include "Position.h"

Position::Position(uint32_t row, uint32_t column) {
  this->row = row;
  this->column = column;
}

Position::Position() : Position(0, 0) {}

bool Position::operator==(const Position &other) const {
  return row == other.row && column == other.column;
}

bool Position::operator!=(const Position &other) const {
  return !(*this == other);
}

Position &Position::operator=(const Position &other) {
  row = other.row;
  column = other.column;
  return *this;
}

Position Position::operator+(const Vector2D &other) const {
  Position new_position{};
  new_position.row = row + other.y;
  new_position.column = column + other.x;
  return new_position;
}

Position Position::operator-(const Vector2D &other) const {
  Position new_position{};
  new_position.row = row - other.y;
  new_position.column = column - other.x;
  return new_position;
}

Position Position::operator+=(const Vector2D &other) {
  return *this = *this + other;
}

Position Position::operator-=(const Vector2D &other) {
  return *this = *this - other;
}

Vector2D Position::distance_to(const Position &other) const {
  int x = column - other.column;
  int y = row - other.row;
  return Vector2D{x, y};
}

