#pragma once

#include "vector_serialiser.h"
#include <cstddef>
#include <iostream>
#include <ostream>
#include <vector>

template <class T>
std::ofstream &operator<<(std::ofstream &out, const std::vector<T> &val) {
  std::size_t size = val.size();

  out.write((char *)&size, sizeof(std::size_t));
  // If it is primitive types (like bool, int, char, etc.)
  if (std::is_fundamental<T>::value) {
    out.write((char *)val.data(), size * sizeof(T));
  } else {
    for (int i = 0; i < size; i++) {
      out << val[i];
    }
  }

  return out;
}

template <class T>
std::ifstream &operator>>(std::ifstream &in, std::vector<T> &val) {
  std::size_t size;
  in.read((char *)&size, sizeof(std::size_t));

  val.resize(size);
  // If it is primitive types (like bool, int, char, etc.)
  if (std::is_fundamental<T>::value) {
    in.read((char *)val.data(), size * sizeof(T));
  } else {
    for (int i = 0; i < size; i++) {
      in >> val[i];
    }
  }

  return in;
}