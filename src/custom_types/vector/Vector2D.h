#pragma once

#include <fstream>

struct Vector2D {
  public:
    int x = 0;
    int y = 0;

    bool operator==(const Vector2D &other) const {
      return x == other.x && y == other.y;
    }

    Vector2D operator+(const Vector2D &other) const {
      Vector2D newVec{};
      newVec.x = x + other.x;
      newVec.y = y + other.y;

      return newVec;
    }

    Vector2D operator-(const Vector2D &other) const {
      Vector2D newVec{};
      newVec.x = x - other.x;
      newVec.y = y - other.y;

      return newVec;
    }

    Vector2D operator*(const int &other) const {
      Vector2D newVec{};
      newVec.x = x * other;
      newVec.y = y * other;

      return newVec;
    }

    Vector2D operator/(const int &other) const {
      Vector2D newVec{};
      newVec.x = x / other;
      newVec.y = y / other;

      return newVec;
    }
};

std::ofstream &operator<<(std::ofstream &out, const Vector2D &vec);
std::ifstream &operator>>(std::ifstream &in, Vector2D &vec);