#pragma once

#include <fstream>
#include <vector>

template <class T>
std::ofstream &operator<<(std::ofstream &out, const std::vector<T> &val);

template <class T>
std::ifstream &operator>>(std::ifstream &in, std::vector<T> &val);

#if !defined(VECTOR_SERIALISE_HPP)
#define VECTOR_SERIALISE_HPP
#include "vector_serialiser.hpp"
#endif