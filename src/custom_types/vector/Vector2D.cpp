#include "custom_types/vector/Vector2D.h"

std::ofstream &operator<<(std::ofstream &out, const Vector2D &vec) {
  out.write((char *)&vec.x, sizeof(int));
  out.write((char *)&vec.y, sizeof(int));

  return out;
}

std::ifstream &operator>>(std::ifstream &in, Vector2D &vec) {
  in.read((char *)&vec.x, sizeof(int));
  in.read((char *)&vec.y, sizeof(int));

  return in;
}