#pragma once

#include <cstdint>
#include <fstream>

enum class GameType : uint8_t {
  MORPION = 1,
  PUISSANCE4 = 2,
  OTHELLO = 3,
  DAMES = 4,
  GAME_COUNT = DAMES
};

std::ofstream &operator<<(std::ofstream &out, const GameType &type);
std::ifstream &operator>>(std::ifstream &in, GameType &type);