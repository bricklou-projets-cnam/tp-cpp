#include "custom_types/game_type/GameType.h"

#include <cstdint>

std::ofstream &operator<<(std::ofstream &out, const GameType &type) {
  out << static_cast<uint8_t>(type);
  return out;
}

std::ifstream &operator>>(std::ifstream &in, GameType &type) {
  uint8_t val;

  if (in >> val)
    type = static_cast<GameType>(val);
  return in;
}