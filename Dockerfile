FROM gcc

RUN apt-get update -y
RUN apt-get install -y cmake
RUN apt-get install -y qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
CMD ["bash"]